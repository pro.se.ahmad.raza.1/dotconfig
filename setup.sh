#!/usr/bin/env bash

CWD=$(pwd)

echo 'Linking tmux configuration'
ln -sf "$CWD/tmux/config" "$HOME/.tmux.conf"

echo 'Installing tpm and tmux plugins'
git clone https://github.com/tmux-plugins/tpm ~/.config/tmux/tpm
tmux source ~/.tmux.conf && tmux run-shell /home/ar/.config/tmux/tpm/bindings/install_plugins


