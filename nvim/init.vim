" VUNDLE """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set rtp+=~/.config/nvim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'morhetz/gruvbox'
" # or 'light' Plugin 'codota/tabnine-vim'
" Plugin 'preservim/nerdtree'
Plugin 'ryanoasis/vim-devicons'

call vundle#end()
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


" VIM PLUG """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin(stdpath('data') . '/plugged')

Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'preservim/nerdtree' |
            \ Plug 'Xuyuanp/nerdtree-git-plugin'

call plug#end()
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


" BASIC EDITOR """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set nocompatible
set syntax
set number
filetype plugin on
filetype indent on
set encoding=UTF-8

set autoread
set incsearch       " Find the next match as we type the search
set hlsearch        " Highlight searches by default
set ignorecase      " Ignore case when searching...
set smartcase       " ...unless we type a capital

set autoindent
set smartindent
set smarttab
set shiftwidth=2
set softtabstop=2
set tabstop=2
set expandtab

nnoremap <C-R> :source ~/.config/nvim/init.vim<CR> " Reload this configuration
nnoremap te :tabedit 
nnoremap tE :tabedit<CR>
nnoremap tl :tabnext<CR>
nnoremap th :tabprevious<CR>
nnoremap tq :tabclose 
nnoremap tQ :tabclose<CR>
nnoremap tL :tabmove +1<CR>
nnoremap tH :tabmove -1<CR>
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


" LOOK AND FEEL """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set background=dark
colorscheme gruvbox
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" TREE - NERDTree """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <C-n> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
